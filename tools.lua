minetest.register_node('real_obsidian:placed_macuahuitl', {
    description = 'Macuahuitl',
    drawtype = 'mesh',
    paramtype = 'light',
    paramtype2 = 'none',
    selection_box = {
        type = 'fixed',
        fixed = { -8 / 16, -8 / 16, -8 / 16, 8 / 16, -7 / 16, 8 / 16 }
    },
    mesh = 'extrusion_mesh_16.obj',
    tiles = { 'real_obsidian_macuahuitl.png' },
    use_texture_alpha = 'clip',
    inventory_image = 'real_obsidian_macuahuitl.png',
    wield_image = 'real_obsidian_macuahuitl.png',
    floodable = true,
    walkable = false,
    sunlight_propagates = true,
    buildable_to = true,
    is_ground_content = false,
    groups = {
        falling_node = 1,
        oddly_breakable_by_hand = 3,
        flammable = 2,
        attached_node = 1,
        dig_immediate = 3,
        snappy = 3,
        sword = 1
    },
    tool_capabilities = {
        full_punch_interval = 0.7,
        max_drop_level = 1,
        groupcaps = {
            snappy = { times = { [1] = 1.90, [2] = 0.90, [3] = 0.30 }, uses = 40, maxlevel = 3 },
        },
        damage_groups = { fleshy = 8 },
    },
    sounds = default.node_sound_wood_defaults() or nil
})

-- minetest.register_tool('real_obsidian:macuahuitl', {
--     description = 'Macuahuitl',
--     inventory_image = 'real_obsidian_macuahuitl.png',
--     tool_capabilities = {
--         full_punch_interval = 0.7,
--         max_drop_level = 1,
--         groupcaps = {
--             snappy = { times = { [1] = 1.90, [2] = 0.90, [3] = 0.30 }, uses = 40, maxlevel = 3 },
--         },
--         damage_groups = { fleshy = 8 },
--     },
--     sound = { breaks = "default_tool_breaks" },
--     groups = { sword = 1 }
-- })

minetest.register_tool('real_obsidian:macuahuitl_broken', {
    description = 'Broken Macuahuitl',
    inventory_image = 'real_obsidian_macuahuitl_broken.png',
    tool_capabilities = {
        full_punch_interval = 0.7,
        max_drop_level = 1,
        groupcaps = {
            snappy = { times = { [1] = 1.90, [2] = 0.90, [3] = 0.30 }, uses = 40, maxlevel = 3 },
        },
        damage_groups = { fleshy = 8 },
    },
    sound = { breaks = "default_tool_breaks" },
    groups = { sword = 1 }
})

-- function spears_register_spear(spear_type, desc, base_damage, toughness, material)
-- spears_register_spear('obsidian', 'Obsidian', 8, 30, 'default:obsidian')

minetest.register_tool("real_obsidian:spear", {
    description = "Obsidian spear",
    wield_image = "spears_spear_obsidian.png^[transform4",
    inventory_image = "spears_spear_obsidian.png",
    wield_scale = { x = 1.5, y = 1.5, z = 1.5 },
    on_secondary_use = function(itemstack, user, pointed_thing)
        real_obsidian.spears_throw(itemstack, user, pointed_thing)
        if not minetest.settings:get_bool("creative_mode") then
            itemstack:take_item()
        end
        return itemstack
    end,
    on_place = function(itemstack, user, pointed_thing)
        real_obsidian.spears_throw(itemstack, user, pointed_thing)
        if not minetest.settings:get_bool("creative_mode") then
            itemstack:take_item()
        end
        return itemstack
    end,
    tool_capabilities = {
        full_punch_interval = 1.5,
        max_drop_level = 1,
        groupcaps = {
            cracky = { times = { [3] = 2 }, uses = 30, maxlevel = 1 },
        },
        damage_groups = { fleshy = 8 },
    },
    sound = { breaks = "default_tool_breaks" },
    groups = { flammable = 1 }
})

local SPEAR_ENTITY = real_obsidian.spears_set_entity(8, 30)

minetest.register_entity("real_obsidian:spear_entity", SPEAR_ENTITY)

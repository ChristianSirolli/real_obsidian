real_obsidian = {
    path = minetest.get_modpath('real_obsidian')
}

dofile(real_obsidian.path .. '/functions.lua')
dofile(real_obsidian.path .. '/nodes.lua')
dofile(real_obsidian.path .. '/craftitems.lua')
dofile(real_obsidian.path .. '/tools.lua')
dofile(real_obsidian.path .. '/crafting.lua')
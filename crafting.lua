-- Default

minetest.register_craft({
    output = "real_obsidian:obsidian",
    recipe = {
        { "real_obsidian:shard", "real_obsidian:shard", "real_obsidian:shard" },
        { "real_obsidian:shard", "real_obsidian:shard", "real_obsidian:shard" },
        { "real_obsidian:shard", "real_obsidian:shard", "real_obsidian:shard" },
    }
})

minetest.register_craft({
    output = "real_obsidian:brick 4",
    recipe = {
        { "real_obsidian:obsidian", "real_obsidian:obsidian" },
        { "real_obsidian:obsidian", "real_obsidian:obsidian" }
    }
})

minetest.register_craft({
    output = "real_obsidian:block 9",
    recipe = {
        { "real_obsidian:obsidian", "real_obsidian:obsidian", "real_obsidian:obsidian" },
        { "real_obsidian:obsidian", "real_obsidian:obsidian", "real_obsidian:obsidian" },
        { "real_obsidian:obsidian", "real_obsidian:obsidian", "real_obsidian:obsidian" },
    }
})

minetest.register_craft({
    type = "cooking",
    output = "real_obsidian:glass",
    recipe = "real_obsidian:shard",
})

minetest.register_craft({
    output = "real_obsidian:shard 9",
    recipe = {
        { "real_obsidian:obsidian" }
    }
})

-- Spears
minetest.register_craft({
    output = 'real_obsidian:spear',
    recipe = {
        { "",            "",            "real_obsidian:shard" },
        { "",            "group:stick", "" },
        { "group:stick", "",            "" }
    }
})

minetest.register_craft({
    output = 'real_obsidian:spear',
    recipe = {
        { "real_obsidian:shard", "",            "" },
        { "",                    "group:stick", "" },
        { "",                    "",            "group:stick" }
    }
})

-- Tools

minetest.register_craft({
    output = 'real_obsidian:macuahuitl',
    recipe = {
        { 'real_obsidian:shard', '',           'real_obsidian:shard' },
        { 'real_obsidian:shard', '',           'real_obsidian:shard' },
        { 'real_obsidian:shard', 'group:wood', 'real_obsidian:shard' }
    }
})

minetest.register_craft({
    output = 'real_obsidian:macuahuitl',
    recipe = {
        { 'real_obsidian:shard', '',                                'real_obsidian:shard' },
        { 'real_obsidian:shard', '',                                'real_obsidian:shard' },
        { 'real_obsidian:shard', 'real_obsidian:macuahuitl_broken', 'real_obsidian:shard' }
    }
})

minetest.register_craft({
    output = 'real_obsidian:macuahuitl_broken',
    recipe = {
        { 'real_obsidian:macuahuitl' }
    },
    replacements = { { "real_obsidian:macuahuitl", 'real_obsidian:shard 6' } },
})

-- Fuel

minetest.register_craft({
    type = "fuel",
    recipe = "real_obsidian:macuahuitl_broken",
    burntime = 6,
})

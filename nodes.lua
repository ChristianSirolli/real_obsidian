-- default

minetest.register_node("real_obsidian:obsidian", {
    description = "Obsidian",
    tiles = { "default_obsidian.png" },
    sounds = default.node_sound_stone_defaults(),
    groups = { cracky = 1, level = 2, lava_heatable = 1 },
    _magma_conduits_heats_to = 'real_obsidian:glow_obsidian'
})

minetest.register_node("real_obsidian:brick", {
    description = "Obsidian Brick",
    paramtype2 = "facedir",
    place_param2 = 0,
    tiles = { "default_obsidian_brick.png" },
    is_ground_content = false,
    sounds = default.node_sound_stone_defaults(),
    groups = { cracky = 1, level = 2 },
})

minetest.register_node("real_obsidian:block", {
    description = "Obsidian Block",
    tiles = { "default_obsidian_block.png" },
    is_ground_content = false,
    sounds = default.node_sound_stone_defaults(),
    groups = { cracky = 1, level = 2 },
})

minetest.register_node("real_obsidian:glass", {
    description = "Obsidian Glass",
    drawtype = "glasslike_framed_optional",
    tiles = { "default_obsidian_glass.png", "default_obsidian_glass_detail.png" },
    use_texture_alpha = "clip", -- only needed for stairs API
    paramtype = "light",
    is_ground_content = false,
    sunlight_propagates = true,
    sounds = default.node_sound_glass_defaults(),
    groups = { cracky = 3 },
})

-- magma_conduits
minetest.register_node("real_obsidian:glow_obsidian", {
    description = "Hot Obsidian",
    _doc_items_longdesc = "Obsidian heated to a dull red glow.",
    _doc_items_usagehelp = "When normal obsidian is heated by lava it is converted into this. Beware when digging here!",
    tiles = { "magma_conduits_glow_obsidian.png" },
    is_ground_content = true,
    sounds = default.node_sound_stone_defaults(),
    groups = { cracky = 1, lava_heated = 1, level = 2, not_in_creative_inventory = 1 },
    _magma_conduits_cools_to = "real_obsidian:obsidian",
    light_source = 6,
    drop = "real_obsidian:obsidian",
})

if minetest.global_exists('xpanes') then
    xpanes.register_pane("obsidian_pane", {
        description = "Obsidian Glass Pane",
        textures = { "default_obsidian_glass.png", "", "xpanes_edge_obsidian.png" },
        inventory_image = "default_obsidian_glass.png",
        wield_image = "default_obsidian_glass.png",
        sounds = default.node_sound_glass_defaults(),
        groups = { snappy = 2, cracky = 3 },
        recipe = {
            { "real_obsidian:glass", "real_obsidian:glass", "real_obsidian:glass" },
            { "real_obsidian:glass", "real_obsidian:glass", "real_obsidian:glass" }
        }
    })
end
if minetest.global_exists('stairs') then
    local function my_register_stair_and_slab(subname, recipeitem, groups, images,
                                              desc_stair, desc_slab, sounds, worldaligntex)
        stairs.register_stair(subname, recipeitem, groups, images, desc_stair,
            sounds, worldaligntex)
        stairs.register_stair_inner(subname, recipeitem, groups, images, "",
            sounds, worldaligntex, "Inner " .. desc_stair)
        stairs.register_stair_outer(subname, recipeitem, groups, images, "",
            sounds, worldaligntex, "Outer " .. desc_stair)
        stairs.register_slab(subname, recipeitem, groups, images, desc_slab,
            sounds, worldaligntex)
    end
    my_register_stair_and_slab(
        "obsidian",
        "real_obsidian:obsidian",
        { cracky = 1, level = 2 },
        { "default_obsidian.png" },
        "Obsidian Stair",
        "Obsidian Slab",
        default.node_sound_stone_defaults(),
        true
    )

    my_register_stair_and_slab(
        "obsidian_brick",
        "real_obsidian:brick",
        { cracky = 1, level = 2 },
        { "default_obsidian_brick.png" },
        "Obsidian Brick Stair",
        "Obsidian Brick Slab",
        default.node_sound_stone_defaults(),
        false
    )

    my_register_stair_and_slab(
        "obsidian_block",
        "real_obsidian:block",
        { cracky = 1, level = 2 },
        { "default_obsidian_block.png" },
        "Obsidian Block Stair",
        "Obsidian Block Slab",
        default.node_sound_stone_defaults(),
        true
    )
end
if minetest.global_exists('doors') then
    doors.register("door_obsidian_glass", {
        tiles = { "doors_door_obsidian_glass.png" },
        description = "Obsidian Glass Door",
        inventory_image = "doors_item_obsidian_glass.png",
        groups = { node = 1, cracky = 3 },
        sounds = default.node_sound_glass_defaults(),
        sound_open = "doors_glass_door_open",
        sound_close = "doors_glass_door_close",
        gain_open = 0.3,
        gain_close = 0.25,
        recipe = {
            { "real_obsidian:glass", "real_obsidian:glass" },
            { "real_obsidian:glass", "real_obsidian:glass" },
            { "real_obsidian:glass", "real_obsidian:glass" },
        },
    })
end
if minetest.global_exists('naturalslopeslib') then
    naturalslopeslib.register_slope("real_obsidian:obsidian", {
            description = "Obsidian",
        },
        500,
        { mapgen = 0.33, place = 0.5 }
    )
end

-- default
minetest.register_alias_force("default:obsidian", "real_obsidian:obsidian")
minetest.register_alias_force("default:obsidian_shard", "real_obsidian:shard")
minetest.register_alias_force("default:obsidianbrick", "real_obsidian:brick")
minetest.register_alias_force("default:obsidian_block", "real_obsidian:block")
minetest.register_alias_force("default:obsidian_glass", "real_obsidian:glass")

-- magma_conduits
minetest.register_alias_force("magma_conduits:glow_obsidian", "real_obsidian:glow_obsidian")

-- ethereal
minetest.register_alias_force("ethereal:obsidian_brick", "real_obsidian:brick")

-- spears
minetest.register_alias_force("spears:spear_obsidian", "real_obsidian:spear")